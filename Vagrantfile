# -*- mode: ruby -*-
# vi: set ft=ruby :

# Vagrantfile API/syntax version. Don't touch unless you know what you're doing!
VAGRANTFILE_API_VERSION = "2"

Vagrant.configure(VAGRANTFILE_API_VERSION) do |config|
  config.vm.box = "ubuntu/trusty64"
#  config.vm.box_url = "https://cloud-images.ubuntu.com/vagrant/trusty/current/trusty-server-cloudimg-amd64-vagrant-disk1.box"

  # determine the script is not running on the unix emulator on windows
  is_windows = (RbConfig::CONFIG['host_os'] =~ /mswin|mingw|cygwin/)

  config.vm.synced_folder ".",          "/vagrant", nfs: !is_windows
  config.bindfs.bind_folder "/vagrant", "/vagrant", :owner => "vagrant", :group => "vagrant"
  config.vm.synced_folder "~/.ssh",     "/vagrant/ssh", nfs: !is_windows
  config.bindfs.bind_folder "/vagrant/ssh", "/home/vagrant/ssh", :owner => "vagrant", :group => "vagrant"
  #config.vm.synced_folder "../viral-giveaway-server", "/vagrant/viral-giveaway-server", nfs: !is_windows 
  #config.bindfs.bind_folder "/vagrant/viral-giveaway-server", "/home/vagrant/app/viral-giveaway-server/current", :owner => "vagrant", :group => "vagrant"
  config.vm.synced_folder "../viral-giveaway-server", "/home/vagrant/app/viral-giveaway-server/current", nfs: !is_windows 
  config.bindfs.bind_folder "/home/vagrant/app/viral-giveaway-server/current", "/home/vagrant/app/viral-giveaway-server/current", :owner => "vagrant", :group => "vagrant"

  # if you need cache apt files: comment out this
  #config.vm.synced_folder "../apt_archives", "/var/cache/apt/archives", nfs: !is_windows
  
  # for nfs setting
  config.vm.network :private_network, ip: "192.168.33.10"

  config.vm.define "vg" do |c|
    c.vm.box = "ubuntu/trusty64"
    c.vm.network "forwarded_port", guest: 3306, host: 13306 # mysql
    c.vm.network "forwarded_port", guest: 8080, host: 8080 # admin
    c.vm.network "forwarded_port", guest: 8081, host: 8081 # api
    c.vm.network "forwarded_port", guest: 8082, host: 8082 # web
    c.vm.network "forwarded_port", guest: 8083, host: 8083 # rp 
  end

  config.vm.provision "ansible" do |ansible|
    ansible.playbook = "site.yml"
    ansible.sudo = true
    ansible.tags = ENV['ANSIBLE_TAGS']
    ansible.groups = {
      "api-servers" => %(vg),
      "admin-servers" => %(vg),
      "db-servers" => %(vg),
      "rp-servers" => %(vg),
      "development:children" => ['api-servers', 'admin-servers', 'db-servers', 'rp-servers'],
    }
  end

  config.vm.provider "virtualbox" do |vb|
    vb.customize ["modifyvm", :id, "--ioapic", "on"]
    vb.customize ["modifyvm", :id, "--memory", "2048"]
    vb.customize ["modifyvm", :id, "--cpus", "2"] 
    # IPv6とDNSでのネットワーク遅延対策で追記
    vb.customize ["modifyvm", :id, "--natdnsproxy1", "off"]
    vb.customize ["modifyvm", :id, "--natdnshostresolver1", "off"]

    # ホスト側と時刻の同期
    vb.customize ["setextradata", :id, "VBoxInternal/Devices/VMMDev/0/Config/GetHostTimeDisabled", 0]
  end

end
