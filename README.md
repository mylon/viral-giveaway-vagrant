# viral-giveaway-vagrant 

## Dependencies

- [Vagrant](http://www.vagrantup.com/)
- [Oracle VM VirtualBox](https://www.virtualbox.org/)

## Before you start

we host all the project on private Bitbucket repository. You will need to setup the SSH keys for Git in order to install the related components required. 
Here is how to generate the SSH key and bind it to your Bitbucket account

https://confluence.atlassian.com/display/BITBUCKET/Set+up+SSH+for+Git


## Setup

```
$ cd /path/to/workspace/
$ git clone git@bitbucket.org:mylon/viral-giveaway-server.git
$ git clone git@bitbucket.org:mylon/viral-giveaway-api-doc.git
$ git clone git@bitbucket.org:mylon/viral-giveaway-vagrant.git
$ cd viral-giveaway-vagrant
$ vagrant up
```

## After Setup
### on local
- Copy your SSH key for Bitbucket

		cp {your id_rsa file path} .ssh/bitbucket_id_rsa
		chmod 600 .ssh/bitbucket_id_rsa

### on vagrant guest
- Restart services

        sudo supervisorctl restart viral-giveaway-server-web && sudo supervisorctl restart viral-giveaway-server-api && sudo supervisorctl restart viral-giveaway-server-admin && sudo supervisorctl restart sidekiq && sudo supervisorctl restart vg-emr-job-worker && sudo supervisorctl restart vg-sendmail-worker
